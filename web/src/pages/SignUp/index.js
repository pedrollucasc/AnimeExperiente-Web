import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

//import "./styles.css"
import api from "../../services/api";

class SignUp extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    error: ""
  };

  handleSignUp = async e => {
    e.preventDefault();
    const { name, email, password } = this.state;
    console.log(this.state)
    if (!name || !email || !password) {
      this.setState({ error: "Preencha todos os dados para se cadastrar" });
    } else {
      try {
        await api.post("/users", { name, email, password });
        this.props.history.push("/");
        
      } catch (err) {
        console.log(err);
        this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
      }
    }
  };

  render() {
    return (
      <div className="Container">
        <form className="Form" onSubmit={this.handleSignUp}>
          <img src="https://logodownload.org/wp-content/uploads/2014/10/netflix-logo-5-1.png" alt="AnEx" />
          {this.state.error && <p>{this.state.error}</p>}
          <input
            type="text"
            placeholder="Nome de usuário"
            onChange={e => this.setState({ name: e.target.value })}
          />
          <input
            type="email"
            placeholder="Endereço de e-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <input
            type="password"
            placeholder="Senha"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <button className="confirm-button" type="submit">Cadastrar grátis</button>
          <hr />
          <Link to="/">Fazer login</Link>
        </form>
      </div>
    );
  }
}

export default withRouter(SignUp);